<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shiprules
 */

/**
 * Copyright © 2015 Amasty. All rights reserved.
 */
namespace Amasty\Shiprules\Block\Adminhtml\Rule\Edit\Tab;

use Amasty\Shiprules\Model\RegistryConstants;
use Amasty\CommonRules\Block\Adminhtml\Rule\Edit\Tab\General as CommonRulesGeneral;

class General extends CommonRulesGeneral
{

    public function _construct()
    {
        $this->setRegistryKey(RegistryConstants::REGISTRY_KEY);
        parent::_construct();
    }

    /**
     * @inheritdoc
     */
    protected function formInit($model)
    {
        $promoShippingRulesUrl = $this->getUrl('sales_rule/promo_quote');
        $promoShippingRulesUrl = '<a href="'.$promoShippingRulesUrl.'">'.__('Promotions / Shopping Cart Rules').'</a>';
        $form = parent::formInit($model);
        $fieldset = $form->getElement('apply_in');
        $fieldset->addField(
            'carriers',
            'multiselect',
            [
                'label' => __('Shipping Carriers'),
                'title' => __('Shipping Carriers'),
                'name' => 'carriers[]',
                'values' => $this->poolOptionProvider->getOptionsByProviderCode('carriers'),
            ]
        );
        $fieldset->addField(
            'methods',
            'textarea',
            [
                'label' => __('Shipping Methods'),
                'title' => __('Shipping Methods'),
                'name' => 'methods',
                'note' => __('One method name per line, e.g Next Day Air'),
            ]
        );
        $fieldset->addField(
            'pos',
            'text',
            [
                'label' => __('Priority'),
                'name' => 'pos',
                'note' => __('If a product matches several rules, the first rule will be applied only.'),
            ]
        );
        return $form;
    }
}