<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Sorting
 */


namespace Amasty\Sorting\Plugin\Catalog;

/**
 * Plugin Config
 * plugin name: AddSortingMethods
 * type: \Magento\Catalog\Model\Config
 */
class Config
{
    /**
     * @var \Amasty\Sorting\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\Sorting\Model\MethodProvider
     */
    private $methodProvider;

    /**
     * @var \Amasty\Sorting\Model\SortingAdapterFactory
     */
    private $adapterFactory;

    /**
     * Config constructor.
     *
     * @param \Amasty\Sorting\Helper\Data                 $helper
     * @param \Amasty\Sorting\Model\MethodProvider        $methodProvider
     * @param \Amasty\Sorting\Model\SortingAdapterFactory $adapterFactory
     */
    public function __construct(
        \Amasty\Sorting\Helper\Data $helper,
        \Amasty\Sorting\Model\MethodProvider $methodProvider,
        \Amasty\Sorting\Model\SortingAdapterFactory $adapterFactory
    ) {
        $this->helper = $helper;
        $this->methodProvider = $methodProvider;
        $this->adapterFactory = $adapterFactory;
    }

    /**
     * Retrieve Attributes array used for sort by
     *
     * @param \Magento\Catalog\Model\Config $subject
     * @param array $options
     *
     * @return array
     */
    public function afterGetAttributesUsedForSortBy($subject, $options)
    {
        return $this->addNewOptions($options);
    }

    /**
     * @param array $options
     *
     * @return array
     */
    private function addNewOptions($options)
    {
        $methods = $this->methodProvider->getMethods();

        foreach ($methods as $methodObject) {
            $code = $methodObject->getMethodCode();
            if (!$this->helper->isMethodDisabled($code) && !isset($options[$code])) {
                $options[$code] = $this->adapterFactory->create(['methodModel' => $methodObject]);
            }
        }

        return $options;
    }

    /**
     * Retrieve Attributes Used for Sort by as array
     * key = code, value = name
     *
     * @param \Magento\Catalog\Model\Config $subject
     * @param array $options
     *
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray($subject, $options)
    {
        if ((bool)$this->helper->getScopeValue('general/hide_best_value')) {
            unset($options['position']);
        }

        return $options;
    }
}
