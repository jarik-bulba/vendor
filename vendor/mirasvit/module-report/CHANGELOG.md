## 1.3.25
*(2018-06-20)*

#### Fixed
* Fast filters are not applied to report data (affects from 1.3.24)

---

## 1.3.24
*(2018-06-19)*

#### Features
* Performance improvement: report columns settings

---

## 1.3.23
*(2018-06-18)*

#### Fixed
* In some cases dashboard is not loaded correctly: use full template name for date JS component

---

## 1.3.22
*(2018-06-08)*

#### Fixed
* Error displaying reports in EE: Field entity_id not exists in table catalog_category_product

---

## 1.3.21
*(2018-06-06)*

#### Fixed
* Timezone issue with date filter
* Issue with totals

---

## 1.3.20
*(2018-06-05)*

#### Fixed
* Report date interval is not considered in emails

---

## 1.3.19
*(2018-06-04)*

#### Fixed
* Store filter

---

## 1.3.18
*(2018-05-16)*

#### Improvements
* Ability to change table relations through custom configs
* Ability to use string fields for fast filters

#### Fixed
* Correctly display totals for concatenated strings

---

## 1.3.17
*(2018-05-11)*

#### Fixed
* Error displaying reports (affects from 1.3.16 of module-report)
* Fix error: Field entity_id not exists in table catalog_category_product

---

## 1.3.16
*(2018-05-10)*

#### Fixed
* Error displaying 'Group of Country' column in the dashboard block
* Display all reports for Email Notification

---

## 1.3.15
*(2018-05-07)*

#### Fixed
* Error rendering reports: 'Access to undeclared static property '

---

## 1.3.14
*(2018-04-25)*


#### Fixed
* Concatenated columns are not displayed, add them to complex columns

---

## 1.3.13
*(2018-04-17)*

#### Fixed
* Error displaying reports: do not use prefix for temporary tables

---

## 1.3.12
*(2018-04-13)*

#### Fixed
* Error rendering 'Sales by Category' and 'Product Performance' reports

---

## 1.3.11
*(2018-04-10)*

#### Fixed
* Issue during fresh installation
* Error displaying reports, do not cache temporary tables mirasvit/module-report[#44](../../issues/44)
* Detailed product report opened from Product Performance report does not display information for specific product mirasvit/module-reports[#40](../../issues/40)

---

## 1.3.10
*(2018-04-04)*

#### Fixed
* Performance

---

## 1.3.9
*(2018-04-03)*

#### Fixed
* Error during report exporting when report sorted by custom column

---

## 1.3.8
*(2018-03-28)*

#### Fixed
* Error rendering report when table names are too long
* Fixed issue when module is already installed in app folder

---

## 1.3.7
*(2018-03-21)*

#### Fixed
* Error displaying dashboard widgets 'No date part found'
* Reports are not shown if custom tables used without prefix

---

## 1.3.6
*(2018-03-16)*

#### Improvements
* Add label for product_id column of sales_order_item table

---

## 1.3.5
*(2018-03-02)*

#### Fixed
* Reports fails on Magento 2.1.*

---

## 1.3.4
*(2018-02-27)*

#### Fixed
* Sometimes the totals are not displayed
* Fast filter of type 'select' is not applied when choosing an option

---

## 1.3.3
*(2018-02-27)*

#### Fixed
* doc block

---

## 1.3.2
*(2018-02-23)*

#### Features
* Report Builder

#### Improvements
* Compatibility with Magento 2.1.x
* trim whitespaces

---

#### Fixed
* Multi installation. "Autoload error: Module 'Mirasvit_Report' ..."

---

## 1.3.1
*(2018-02-14)*

#### Fixed
* Issue with empty relations

---

## 1.3.0
*(2018-02-09)*

#### Fixed
* Small modifications
* no--min
* GEO
* Issues with export
* Issue with country flags

---


## 1.2.27
*(2017-12-07)*

#### Fixed
* filters by "Customers > Products" and "Abandoned Carts > Abandoned Products" columns

---

## 1.2.26
*(2017-12-06)*

#### Fixed
* filter by "Products" column

---

## 1.2.25
*(2017-12-05)*

#### Fixed
* Issue with active dimension column

---

## 1.2.24
*(2017-11-30)*

#### Fixed
* Issue with export in Magento 2.1.8

---

## 1.2.23
*(2017-11-27)*

#### Fixed
* Issue with "Total" value of non-numeric columns

---

## 1.2.22
*(2017-11-15)*

#### Fixed
* Issue with export to XML

---

## 1.2.21
*(2017-11-03)*

#### Fixed
* Properly replicate temporary tables
* An issue with builing relations
* Issue with finding way to join tables

---

## 1.2.20
*(2017-10-30)*

#### Fixed
* An issue with sales overview report when customer segments used

---

## 1.2.19
*(2017-10-30)*

#### Fixed
* Issue with export to CSV (Magento 2.1.9)

---

## 1.2.18
*(2017-10-26)*

#### Fixed
* Issue with long replication

---

## 1.2.17
*(2017-10-20)*

#### Fixed
* Fixed css bug
* Compare for leap year

---

## 1.2.16
*(2017-09-28)*

#### Fixed
* Compatibility with php 7.1.9

---

## 1.2.15
*(2017-09-26)*

#### Fixed
* M2.2

---


## 1.2.14
*(2017-09-18)*

#### Fixed
* Fix report email notification using 'Send Now' function

---

## 1.2.13
*(2017-08-09)*

#### Fixed
* Conflict with other reports extensions

---

## 1.2.12
*(2017-08-02)*

#### Improvements
* New Report Columns

---

## 1.2.11
*(2017-07-19)*

#### Fixed
* Display option labels instead of values for dashboard widgets

---

## 1.2.10
*(2017-07-12)*

#### Fixed
* Issue with Eav attributes

---

## 1.2.9
*(2017-07-11)*

#### Improvements
* New Charts

---

## 1.2.8
*(2017-06-21)*

#### Fixed
* Proper filter product details report by current product ID

## 1.2.7
*(2017-06-21)*

#### Improvements
* Refactoring

---

## 1.2.6
*(2017-06-01)*

---

## 1.2.5
*(2017-05-31)*

#### Improvements
* Added field to relation

---

## 1.2.4
*(2017-05-15)*

#### Fixed
* Issue with column ordering

---

## 1.2.3
*(2017-05-04)*

#### Bugfixes
* Fixed an issue with compound columns of type simple

#### Improvements
* Changed default multiselect control to ui-select
* Chart resizing

---

## 1.2.2
*(2017-03-21)*

#### Improvements
* Performance

#### Fixed
* Fixed an issue with join returing customers

---

## 1.2.1
*(2017-03-06)*

#### Improvements
* Disabled wrong filters for day/hour/month/quarter/week/year

#### Fixed
* Fixed an issue with table joining
* Fixed an issue with filters
* Issue with rounding numbers in chart

---

## 1.2.0
*(2017-02-27)*

#### Fixed
* Minor issues
* Fixed an issue with replication

---

## 1.1.14
*(2017-01-31)*

#### Fixed
* Dashboard

---

## 1.1.12
*(2017-01-25)*

#### Fixed
* Backward compatibility
* Fixed an issue with bookmarks

---

## 1.1.11
*(2017-01-20)*

#### Fixed
* fixed an issue with tz

---

## 1.1.9, 1.1.10
*(2017-01-13)*

#### Fixed
* Fixed an issue with timezones
* Fixed an issue with dates


## 1.1.7, 1.1.8

*(2016-12-15)*

#### Fixed
* Fixed an issue in toolbar
* Fixed an issue with date filter

---

## 1.1.6
*(2016-12-09)*

#### Improvements
* Compatibility with M2.2

---

## 1.1.5
*(2016-09-27)*

#### Fixed
* Fixed an issue with moment js

---

## 1.1.4
*(2016-09-13)*

#### Fixed
* Removed limit on export reports (was 1000 rows)

---

## 1.1.3
*(2016-09-05)*

#### Improvements
* Changed product type column type

---

## 1.1.2
*(2016-09-01)*

#### Improvements
* Added Product Type column

---

## 1.1.1
*(2016-08-15)*

#### Fixed
* Fixed an issue with exporting

---

## 1.1.0
*(2016-07-01)*

#### Fixed
* Rename report.xml to mreport.xsd (compatiblity with module-support)

---

## 1.0.4
*(2016-06-24)*

#### Fixed
* Compatibility with Magento 2.1

---

## 1.0.3
*(2016-05-31)*

#### Fixed
* Fixed an issue with currency symbol

---

## 1.0.2
*(2016-05-27)*

#### Fixed
* Add store filter

---

## 1.0.1
*(2016-05-25)*

#### Fixed
* Removed font-awesome

---

## 1.0.0
*(2016-05-19)*

#### Improvements
* Export
* Refactoring
* Table join logic

#### Fixed
* Fixed an issue with joining tables
* Chart - multi columns
