define([
    "jquery",
    "jquery/ui",
    "Amasty_Conf/js/elevatezoom/jquery.elevatezoom",
    "Amasty_Conf/js/fancybox/jquery.fancybox",
    "Amasty_Conf/js/slick/slick.min",
    'uiClass'
], function ($, ui, elevatezoom, fancybox, slick, Class) {

    return Class.extend({

        defaults: {
            settings: {},
            config: {},
            startConfig: {},
            mainImageSelector: '#amasty-main-image',
            galleryImagesSelector: '[data-gallery-role="amasty-gallery-images"]',
            galleryContainerSelector: '[data-gallery-role="amasty-gallery-container"]',
            mainImageContainerSelector: '[data-gallery-role="amasty-main-container"]',
            loadingSelector: '[data-gallery-role="gallery-loading"]'
        },

        /**
         * Initializes gallery.
         * @param {Object} config - Gallery configuration.
         * @param {String} element - String selector of gallery DOM element.
         */
        initialize: function (config, element) {
            this.insertPolyfills();
            var self = this,
                gallery = $('[data-gallery-role=gallery-placeholder]', '.column.main');
            this._super();
            this.config = config;
            this.config.origData = this.config.data.slice(0);
            this.config.modifiedData = this.config.origData.slice(0);
            $.each(this.config.modifiedData, function(index, item) {
                this.config.modifiedData[index] = Object.assign({}, item);
                this.config.modifiedData[index]['isMain'] = false;
            }.bind(this));

            this.element = element;
            this.galleryImages = $(this.galleryImagesSelector);

            if (this.isMobileAndTablet()) {
                this.config.options.zoom['zoomWindowPosition'] = 6;//th best position on the bottom of image
                this.config.options.zoom['zoomWindowWidth'] = 270;//the best choise for mobile
                this.config.options.zoom['zoomWindowHeight'] = 270;
            }

            this.generateProductImages();
            this.load();
            $(element).data('zoom_object', this);
            gallery.data('amasty_gallery', true);
            gallery.trigger('amasty_gallery:loaded');
        },

        reload: function (images, gallerySwitchStrategy) {
            var initialImages = [];
            if (!images || _.isEmpty(images[0])) {
                initialImages = this.config.origData;
            } else if (gallerySwitchStrategy == 'prepend') {
                initialImages = this.config.modifiedData;
            }
            this.config.data = $.merge($.merge([], images), initialImages);
            this.showPreloading();
            this.destroyImages();

            this.generateProductImages();
            this.load();
        },

        destroyImages: function () {
            $(this.mainImageContainerSelector).html('');
            if (this.galleryImages.find('a').length > this.config.options.carousel.slidesToShow) {
                try{
                    this.galleryImages.slick('unslick');
                } catch(e) {
                    //sometimes for slow connections using 'unslick' option could break
                    // js script on a page. It shouldn't work before all content loaded
                }

            }
            this.galleryImages.html('');
        },

        load: function () {
            var element = $(this.mainImageSelector);
            if (element) {
                var generalSettings = this.config.options.general;

                if (generalSettings.zoom || generalSettings.lightbox) {
                    this.loadZoom(element);
                }

                if (generalSettings.lightbox) {
                    this.loadLightbox(element);
                }

            } else {
                console.log('There are something wrong. The are not main product image')
            }

            if (generalSettings.carousel) {
                this.loadCarousel();
            }
        },

        loadZoom: function (element) {
            var self = this;
            if (this.isMobileAndTablet() && this.config.options.carousel.main_image_swipe) {
                this.config.options.zoom.zoomType = null;
                element.swipeleft(function() {
                    self.swipeMainImage($(this), 'next');
                });

                element.swiperight(function() {
                    self.swipeMainImage($(this), 'prev');
                });
            }

            element.elevateZoom(this.config.options.zoom);
        },

        swipeMainImage: function (element, type) {
            var button = $('.slick-' + type),
                newImage = null,
                imageData = element.attr('src'),
                smallImage = $('.amasty-gallery-thumb-link[data-image="' + imageData + '"]:not(.slick-cloned)'),
                eventType = this.config.options.zoom.image_change === 'hover' ? 'mouseover' : 'click';

            if (button.length) {
                button.trigger('click');
                $('.slick-current').trigger(eventType);
            } else {
                if (smallImage.length) {
                    if (type === 'prev') {
                        newImage = smallImage.prev();
                    } else {
                        newImage = smallImage.next();
                    }

                    newImage.trigger(eventType);
                }
            }
        },

        isMobileAndTablet: function () {
            var check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

            return check;
        },

        loadLightbox: function (element) {
            var self = this;
            var galleryObject = element.data('elevateZoom');
            element.parent().addClass('am-custor-pointer');
            element.parent().unbind("click").bind("click", function(e) {
                $.fancybox.open(galleryObject.getGalleryList(), self.config.options.lightbox);
                return false;
            });

            if(this.config.options.general.thumbnail_lignhtbox) {
                $(this.galleryContainerSelector + ' a').addClass('cursor-pointer').bind("click", function(e) {
                    var currentZoomImage = galleryObject.zoomImage;

                    galleryObject.zoomImage = $(this).data('zoom-image');
                    $.fancybox.open(galleryObject.getGalleryList(), self.config.options.lightbox);
                    galleryObject.zoomImage = currentZoomImage;
                });
            }
        },
        
        
        loadCarousel: function () {
            var self = this,
                imageCount = this.galleryImages.find('a').length,
                firstImage = this.galleryImages.find('a img').first();

            if (!imageCount) {
                return;
            }

            if ($(firstImage).height() > 0) {
                //images are loaded
                var config = this.config.options.carousel;
                config.speed = 300;
                if ($('body').css('direction') == 'rtl') {
                    config.rtl = true;
                }
                /* if images count is more than slides to show*/
                if (imageCount > config.slidesToShow || this.isMobileAndTablet()) {
                    this.galleryImages.removeClass('slick-initialized');
                    this.galleryImages.slick(config);
                }
            }
        },

        hidePreloading: function () {
            var preloader = $(this.loadingSelector);
            if (preloader.length) {
                preloader.hide();
            }
        },

        showPreloading: function () {
            var preloader = $(this.loadingSelector);
            if (preloader.length) {
                preloader.show();
            }
        },

        generateMainImage: function (imageObject) {
            var mainImageContainer = $(this.mainImageContainerSelector);
            if (mainImageContainer.length
                && (imageObject.type === 'image'
                || (imageObject.type === undefined && imageObject.img))
            ) {
                var element = $('<img>',{
                    id:  'amasty-main-image',
                    'data-zoom-image': imageObject.full,
                    class: 'amasty-main-image',
                    title: imageObject.caption,
                    alt: imageObject.caption,
                    src: imageObject.img
                });

                mainImageContainer.append(element);
                if (typeof(this.config.options.zoom.medium_size.width) !== "undefined") {
                    mainImageContainer.css({
                        width: this.config.options.zoom.medium_size.width
                    });
                }
            }
        },

        generateProductImages: function () {
            var self = this,
                galleryImagesContainer = this.galleryImages,
                mainImageGenerated = false;
            if (this.config.data.length && galleryImagesContainer.length) {
                $('#amasty-gallery').addClass('position-' + this.config.options.general.carousel_position);
                $.each(this.config.data, function(key, imageObject) {
                    if (imageObject.thumb) {
                        var element = $('<img>',{
                            class: 'amasty-gallery-image',
                            title: imageObject.caption,
                            alt: imageObject.caption,
                            width: self.config.options.zoom.small_size.width,
                            src: imageObject.thumb
                        });
                        
                        element.load(function() {
                            if (!self.galleryImages.hasClass('slick-initialized')) {
                                self.loadCarousel();
                            }
                        });

                        var link = $('<a>',{
                            class: 'amasty-gallery-thumb-link',
                            'data-image': imageObject.img,
                            'data-zoom-image': imageObject.full,
                            title: imageObject.caption,
                            rel: 'amasty-gallery-group',
                            css: {
                                'position': 'relative'
                            }
                        });

                        if (imageObject.type === 'video'
                            && imageObject.videoUrl
                        ) {
                            link.attr('data-video-url', imageObject.videoUrl);
                            link.addClass('video-thumb-icon');
                        }

                        link.append(element);
                        galleryImagesContainer.append(link);

                        if (imageObject.isMain) {
                            mainImageGenerated = true;
                            self.generateMainImage(imageObject);
                        }

                    }
                });
                if (!mainImageGenerated) {
                    self.generateMainImage(this.config.data.first());
                }

                if (this.config.options.zoom.image_change) {
                    $(this.galleryContainerSelector + ' a').addClass('cursor-pointer')
                }

            } else {
                console.log('There are no images for this product or selector is wrong.');
            }

            this.hidePreloading();
        },

        insertPolyfills: function () {
            if (typeof Object.assign != 'function') {
                // Must be writable: true, enumerable: false, configurable: true
                Object.defineProperty(Object, "assign", {
                    value: function assign(target, varArgs) { // .length of function is 2
                        'use strict';
                        if (target == null) { // TypeError if undefined or null
                            throw new TypeError('Cannot convert undefined or null to object');
                        }

                        var to = Object(target);

                        for (var index = 1; index < arguments.length; index++) {
                            var nextSource = arguments[index];

                            if (nextSource != null) { // Skip over if undefined or null
                                for (var nextKey in nextSource) {
                                    // Avoid bugs when hasOwnProperty is shadowed
                                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                                        to[nextKey] = nextSource[nextKey];
                                    }
                                }
                            }
                        }
                        return to;
                    },
                    writable: true,
                    configurable: true
                });
            }
        }
    })
});
