<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    const MODULE_PREFIX = 'amasty_gdpr/';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return array
     */
    public function getEEACountryCodes()
    {
        $codes = explode(',', $this->getValue('eea_countries'));

        return $codes;
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public function getValue($path)
    {
        return $this->scopeConfig->getValue(
            self::MODULE_PREFIX . $path,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function isSetFlag($path)
    {
        return $this->scopeConfig->isSetFlag(
            self::MODULE_PREFIX . $path,
            ScopeInterface::SCOPE_STORE
        );
    }
}
