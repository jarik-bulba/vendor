<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shiprules
 */


namespace Amasty\Shiprules\Model;

class ProductRegistry extends \Amasty\CommonRules\Model\ProductRegistry
{}